const GoogleFontsPlugin = require("google-fonts-webpack-plugin")

module.exports = {
	// 為了讓包完以後的 html 讀得到靜態檔
  publicPath: './',
  // webpack 設定
  chainWebpack: config => {
  	// 掛載 plugins
    plugins: [
      // google web fonts
      new GoogleFontsPlugin({ fonts: [ { family: "Rubik" } ] })
    ]
   }
}
