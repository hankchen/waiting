/** waiting(候位) 專用的 function */
// import store from './store'
import { L_Obj } from './s_obj.js'



/* 特殊企業號(key:相對應的資料匣名稱) */
const EnterpriseIDs = {
	tzml: ['53855259','42819072','64922255'] //碳佐相關企業號  
}

/* 拿取企業號對應的名稱 */
function GetEnterpriseKey(in_EnterpriseID) {
	let currentEnterpriseKey = 'pages';
	Object.entries(EnterpriseIDs).some(([key, value]) => {
		if (value.includes(in_EnterpriseID)) {
			currentEnterpriseKey = key
			return true
		}
	});
	return currentEnterpriseKey;
}

/* 拿取企業號在網址,並判斷是否為特殊的企業號 */
//ex: #/r/53855259?i=1616548076
function GetEnterpriseKeyFromUrlPath() {
	let regex = /#\/r\/(.*?)\?i=\d+/gm//判斷是否是掃QR Code進來
	let EnterpriseKey = L_Obj.read('themeVar')
	let hash = regex.exec(window.location.hash)
	if (hash) {
		let matchesArr = [...hash]
		if (matchesArr.length >= 2 && !!matchesArr[1]) {
			EnterpriseKey = EnterpriseKey || GetEnterpriseKey(matchesArr[1])
		}
	}
	return EnterpriseKey
}


export {
	GetEnterpriseKey, GetEnterpriseKeyFromUrlPath
}