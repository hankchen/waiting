import Vue from 'vue'
import VueRouter from 'vue-router'
import { GetEnterpriseKeyFromUrlPath } from '../f_waiting.js'
import { L_Obj } from '../s_obj.js'
//import { deviseFunction} from '../libs/deviseHelper.js'
Vue.use(VueRouter)

// vue router 設定
let themeVar = 'pages' // 預設
let currentEnterpriseKey = GetEnterpriseKeyFromUrlPath();
if (currentEnterpriseKey!=null && currentEnterpriseKey!='') {  
  themeVar = currentEnterpriseKey;
}

//將風格存起來
L_Obj.save('themeVar', themeVar);


const routes = [
  { path: '/',		      		component: () => import(`../${themeVar}/Index.vue`)},
  { path: '/login',      		component: () => import(`../${themeVar}/Login.vue`)},
  { path: '/number_taker',	component: () => import(`../${themeVar}/NumberTaker.vue`)},
  { path: '/dashboard', 		component: () => import(`../${themeVar}/Dashboard.vue`)},
  { path: '/kanban',    		component: () => import(`../${themeVar}/Kanban.vue`)},
  { path: '/r/:id',	    		component: () => import(`../${themeVar}/Reservations.vue`)},
]

export default new VueRouter({routes})
