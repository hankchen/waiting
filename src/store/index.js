/* eslint-disable */

// Vue libs
import Vue from 'vue'
import Vuex from 'vuex'
import VueCookies from 'vue-cookies'
import { L_Obj, GetPara1 } from '../fun1.js'	// 共用fun在此!
Vue.use(Vuex)
Vue.use(VueCookies)

// custom & 3rd libs
import axios from 'axios'
import md5 from 'md5'
import { moment, getToday } from '../libs/timeHelper.js'
// 與殼溝通相關的 function
import { deviseFunction } from '../libs/deviseHelper.js'
import { showAlert } from '../libs/appHelper.js'
//router
import router from '../router/index.js'
axios.defaults.timeout = 15000;

/* 值(字串)左邊補0(預設) */
const PadLeft = function(str, len, fill) {
	str = '' + str;	// 轉字串
	fill || (fill = '0');
	const nMax	= str.length
	return nMax >= len ? str : new Array(len - nMax + 1).join(fill) + str;
};
/** @Sample:
	PadLeft('1', 2);			// 會輸出 01
	PadLeft('a', 3, '*');		// 會輸出 **a
*/

const IsDev = /localhost/.test(location.host) || /^192.168/ig.test(location.host)
// const IsDev = process.env.NODE_ENV === 'development'


export default new Vuex.Store({
// 整個前端專案的 『data 設定/變化/異步』 統一由此控管
  // 基礎資料
  state: {
    version: '20211217-01',	// buuild version(做版號
    // example => 用法: this.$store.state.isLoading
    // 給 state 用的 promise
    promise: null, // 保存promise
    // 是否在殼裡面(判斷能否使用殼相關接口的依據)
    isDeviseApp: false,
    // 啟動/關閉 『讀取中』的動畫效果
    isLoading: false,
	// 是否本機開發
    isDev: IsDev,
    // 判斷是否已完成前端初始化
    isLoaded: false,
    // 網路狀態異常標記
    networkError:false,
    // 圖檔 / 靜態檔的 source url
    // 無圖時要顯示的圖檔
    noImageUrl: 'https://dev-service.jinher.com.tw/projectFeature/APP_JG_SSG/JH_APP/images/imageError.png',
    // 客製的彈窗 (用於 alert / confirm)
    customModal: {
      type: '', // 'alert' || 'confirm'
      text: '',
      cancelText: '取消',
      confirmText: '確認'
    },
    // CSS 全域參數(用於判斷 CSS 是否設定 -webkit-overflow-scrolling: touch)
    // true => touch,
    // false => auto
    cssTouch: true,
    // 會員登入頁的表單
    loginForm: { submited: false, EnterpriseID: '', account: '', password: '', rememberMe:  true },
    // 基本資訊
    baseInfo: {
      // 語系 (預設值：'TW')
      lang: 'tw',
      // 是否接收推播訊息 (跟殼要)
      pushNotify: false,
      // GPS 資訊 (跟殼要)
      gps: {},
      // 螢幕亮度值 (跟殼要, range 0 ~ 255 預設為中間值)
      brightness: '120',
      baseHoHttp: (L_Obj.read('baseHoHttp') || 'https://8012.jh8.tw'), //https://9001.jh8.tw  ,登入時會給
    },
    // 簡訊認證設定 (需要從殼取)
    SMS_Config: {
      // 簡訊回傳的驗證碼
      reCode: "",
      // 每天最多簡訊次數 (預設為 5 次)
      APPSMSDayTimes: '5',
      // 每封簡訊發送間隔 (預設為 50 秒)
      AppSMSIntervalMin: '50',
      // 每日簡訊發送扣打 (用來紀錄已寄出幾封, 不得超過 APPSMSDayTimes 的值), 格式： yyyymmdd-times => e.g. 20190513-4 (05/13 已用四次)
      SMSSendQuota: '',
    },
    // 出單機 ip
    prinip: VueCookies.get('prinip') || '192.168.1.101',
    // 單據格式資料夾
    prinFolder: 'default',
    // API url
    // reqUrl: L_Obj.read('baseHoHttp')?`${L_Obj.read('baseHoHttp')}/APPWait.ashx`:`https://8012.jh8.tw/Public/APPWaitOneEntry.ashx`,		// 直達會跨域
    reqUrl: 'https://8012.jh8.tw/Public/APPWaitOneEntry.ashx',		// PS: 20211206-秋哥移機
    //reqUrl: 'http://8012test.jh8.tw/Public/APPWaitOneEntry.ashx',	// @@
    // reqUrl: 'https://waiting.jh8.tw/Public/APPWaitOneEntry.ashx',

    // API head
    reqHead: { "headers": { "Content-Type": "application/x-www-form-urlencoded" } },
    // navbar 上的主標
    headTitle: '候位管理系統',
    // navbar 上的名稱
    headName: '候位管理系統',
    // 看板的桌面通知
    deskNotify: '',
    // 看板的Banner
    kanbanBanner: [],
    // 桌型清單
    deskTypeList: [],
    // 候位資料的備註項目
    waitTagList: [],
    // 所有候位資料
    waitQueueList: [],
    // 門店設定
    waitConfig: {},
    // 基礎資料
    baseData: {
      userCode: VueCookies.get('userCode') || '',
      userName: VueCookies.get('userName') || '',
      EnterpriseID: VueCookies.get('EnterpriseID') || '',
      EnterpriseName: VueCookies.get('EnterpriseName') || '',
      ShopID: VueCookies.get('ShopID') || '',
      ShopTel: VueCookies.get('ShopTel') || '',
      ShopName: VueCookies.get('ShopName') || '',
      ShopAddr: VueCookies.get('ShopAddr') || '',
      MealCount: 1,   // 候位餐期(預設1),
      entryPath: '/', // 進入位置(用來設置登入後要導向的地方),
      todoTitle: '',  // 設定 title (用來切換主色系)
    },
  },
  // 唯一可以改變 store.state 的方法設定 (必須同步執行)
  // Policy:
  //   1. !!重要!! 在每一個 function 上面務必要寫註解，說明此 function 的用意
  //   2. 除非有特殊情況，不然一律以 set 為開頭 (因為這裡 function 目的都是為了 state 的變更)
  mutations: {
  // example => 用法: this.$store.commit('setLoading', true)
    setNetworkError(state, status){ state.networkError = status },
    // 設定 cssTouch
    setCssTouch(state, status){ state.cssTouch = status },
    // 設定是否在殼裡面
    setIsDeviseApp(state, status) { state.isDeviseApp = status },
    // 『讀取中...』 切換
    setLoading(state, status) { state.isLoading = status },
    setLoaded(state, status) { state.isLoaded = status },
    // 設定彈跳視窗的資料 (顯示 / 隱藏)
    setCustomModal(state, { type, text, cancelText, confirmText, resolve, reject }) {
      cancelText = cancelText || '取消'
      confirmText = confirmText || '確認'

      state.customModal = { type, text, cancelText, confirmText }
      if (resolve && reject) state.promise = { resolve, reject }
    },
    // 存入手機簡訊認證的設定
    setSMS_Config(state, data) { state.SMS_Config = Object.assign({}, state.SMS_Config, data) },
    // 設定簡訊驗證碼
    // setSMSreCode(state, data) { state.SMS_Config.reCode = data },
    setSMSreCode(state, data) {
		console.log('setSMSreCode-data: ', data);	// @@
		alert('setSMSreCode-data: '+data);
    	state.SMS_Config.reCode = data
    },
    // 存入本地HoHttp,取非登入資料用
    setBaseHoHttp(state, val) { 
      state.baseInfo.baseHoHttp = val;
      L_Obj.save('baseHoHttp',val) 
    },    
    // 存入基本資訊
    setBaseInfo(state, data) { state.baseInfo = Object.assign({}, state.baseInfo, data) },
    setPrinip(state, ip) {
    	state.prinip = ip
		VueCookies.set('prinip', ip)
    },
    setPrinFolder(state, folder) { state.prinFolder = folder },
    setEnterpriseID(state, id) { state.baseData.EnterpriseID = id },
    setShopID(state, id) { state.baseData.ShopID = id },
    setShopName(state, name) { state.baseData.ShopName = name },
    // 設定頁面主標
    setHeadTitle(state, title) { state.headTitle = title },
    // 設定頁面名稱
    setHeadName(state, name) { state.headName = name },
    // 設定進入位置(登入後要導向的地方)
    setEntryPath(state, path) {
      const entryPath = (path === '/login') ? '/' : path
      L_Obj.save("entryPath",entryPath)
      state.baseData.entryPath = entryPath
    },
    setTodoTitle(state, name) { state.baseData.todoTitle = name },
    setDeskNotify(state, code) { state.deskNotify = code },
    // 加入新增的候位資料
    setWaitQueueList(state, data) {
      app.home.$set(state.waitQueueList, state.waitQueueList.length, data)
    },
    // 存入看板Banner
    setKanbanBanner(state, data) { state.kanbanBanner = data },
    // 存入桌型清單
    setWaitingDesk(state, data) { state.deskTypeList = data },
    // 存入候位標籤
    setWaitingTag(state, data) { state.waitTagList = data },
    // 存入候位資料
    setWaitingQueue(state, data) { state.waitQueueList = data },
    // 存入餐期資料
    setMealCount(state, data) { state.baseData.MealCount = data },
    // 存入門店設定
    setWaitConfig(state, data) { state.waitConfig = data },
    // 存入登入資訊
    setLogin(state, data) {
      const {EnterpriseID, EnterpriseName, ShopID, ShopName, userName, userCode, ShopTel, ShopAddr} = data
      const Eid = EnterpriseID.trim() // EnterpriseID.trim().toLowerCase()
      const Sid = ShopID ? ShopID.trim() : '' // ShopID ? ShopID.trim().toLowerCase() : ''
      const Sname = ShopName ? ShopName.trim() : '' // ShopName ? ShopName.trim().toLowerCase() : ''
      VueCookies.set('EnterpriseID', Eid || '')
      VueCookies.set('EnterpriseName', EnterpriseName || '')
      VueCookies.set('userCode', userCode || '')
      VueCookies.set('userName', userName || '')
      VueCookies.set('ShopID', Sid || '')
      VueCookies.set('ShopTel', ShopTel || '')
      VueCookies.set('ShopName', Sname || '')
      VueCookies.set('ShopAddr', ShopAddr || '')

      state.baseData.EnterpriseName = EnterpriseName || ''
      state.baseData.EnterpriseID = Eid || ''
      state.baseData.userCode = userCode || ''
      state.baseData.userName = userName || ''
      state.baseData.ShopID = ShopID || ''
      state.baseData.ShopTel = ShopTel || ''
      state.baseData.ShopName = ShopName || ''
      state.baseData.ShopAddr = ShopAddr || ''

      // 存入企業號
      deviseFunction('SetSP', JSON.stringify({spName: "EnterpriseID", spValue: Eid }))
      if (ShopID) deviseFunction('SetSP', JSON.stringify({spName: "ShopID", spValue: Sid }))
      if (ShopName) deviseFunction('SetSP', JSON.stringify({spName: "ShopName", spValue: Sname }))
    }
  },
  // 資料處理設定 (類似 vue 裡的 computed || decorator)
  // Policy:
  //   1. !!重要!! 在每一個 function 上面務必要寫註解，說明此 function 的用意
  //   2. 只要回傳是 true / false 的一律用 is 開頭
  getters: {
    getStateVersion: state => {
      return state.version
    },
    isLogin: state => {
      const {EnterpriseID, ShopID} = state.baseData
      if (EnterpriseID !== '' && ShopID !== '') return true
      return false
    },
    // 建立候位資料的基本 requestBody
    queueBody: state => {
      const {EnterpriseID, ShopID, MealCount} = state.baseData
      return { ShopID, MealCount, EnterpriseID }
    },
    // 目前各桌的最後號次
    currentDeskNotifyNumber: state => {
      const {MealCount} = state.baseData
      const {deskTypeList, waitQueueList} = state

      return deskTypeList.map(desk => {
        const deskQueue = waitQueueList.filter(queue => queue.DeskTypeID === desk.GID && queue.MealCount === MealCount)
        const lastNumber = (deskQueue.length > 0) ? deskQueue.sort((a, b) => b.Number - a.Number)[0].Number : 0
        return {code: desk.DeskTypeCode, lastNumber: lastNumber}
      })
    }
  },
  // 組合執行 mutations 的地方(可異步，讓資料變更與異步邏輯切分)
  // Policy:
  //   1. !!重要!! 在每一個 function 上面務必要寫註解，說明此 function 的用意
  //   2. !!重要!! 務必要在 function 特殊要注意的地方加上 debug message, 方便以後偵錯 (頭尾 logger 已用 plugin 方式塞入)
  //   3. 所有 fetch  開頭的都是跟 request 相關 (e.g. 打 API 取資料)
  //   4. 所有 ensure 開頭的都是用 promise 寫的，使用時注意 .then / .catch
  //   5. 所有 devise 開頭的都是跟 殼(ios, andriod) 相關
  //   6. 所有 member 開頭的都是跟 會員 相關
  actions: {
  // example => 用法: this.$store.dispatch('fetchInitData')
    // (藉由殼 API) 發送手機驗證簡訊
    // sendDeviseSMSreCode({dispatch, commit, state, getters}, payload) {
      // // payload sample: { account: '0955111222', SMSSendQuota: '20190514-3' }
      // // 將已發送扣打紀錄存入殼裡
      // deviseFunction('SetSP', `{"spName":"SMSSendQuota", "spValue":"${payload.SMSSendQuota}"}`, '')
      // // 發送手機驗證簡訊
      // deviseFunction('AppSMSReCode', payload.account, '"cbFnSyncDeviseSMSreCode"')
    // },
    // 發送 WebSocket 推播訊息
    cablePublish({dispatch, commit, state, getters}, message) {
      // 資料驗證
      const {EnterpriseID, ShopID, userName} = state.baseData
      if (!this._vm.$cable || !EnterpriseID || !ShopID) return

      // 設定要推送的資料
      let payload = { EnterpriseID, ShopID, userName, type: 'waiting', act: 'reload' }
      if (typeof(message) === 'string') payload = Object.assign({}, payload, {message} )
      if (typeof(message) === 'object') payload = Object.assign({}, payload,  message )

      return this._vm.$cable.commit(payload)
    },
    // 取得門店設定
    async fetchWaitConfig({dispatch, commit, state, getters}) {
      commit('setLoading', true)
      const reqBody = Object.assign({}, getters.queueBody, {act: "getwaitingsettingshop"})
      const {reqUrl, reqHead} = state
      const datatmp 	= await axios.post(reqUrl, reqBody, reqHead).catch(err => {
        if (/Network Error|timeout/.test(err)) commit('setNetworkError', true);
      })
      commit('setLoading', false)
      if (!datatmp) return; //萬一datatmp.data沒有值時.
      const  {data} = datatmp;
      if (data.ErrorCode !== '0') return
      if (!data.Remark || typeof(data.Remark) !== 'string') return; //萬一Remark沒有值時.
      const configtmp = JSON.parse(data.Remark)
      if (!Array.isArray(configtmp) || (Array.isArray(configtmp) && !configtmp.length) ) return;
      const config = configtmp[0]
      // console.log('fetchWaitConfig-config: ', config);	// @@(微管雲設定
      if (config && config.ParValue) commit('setWaitConfig', JSON.parse(config.ParValue))
    },
    // 建立候位資料
    async createWaitingQueue({dispatch, commit, state, getters}, body) {
      commit('setLoading', true)
      const reqBody = Object.assign({}, getters.queueBody, body)
      const {EnterpriseID, EnterpriseName, ShopName, ShopTel, ShopID} = state.baseData
      const {MealCount, DeskID, Number} = reqBody
      const {reqUrl, reqHead} = state
      const datatmp 	= await axios.post(reqUrl, reqBody, reqHead).catch(err => {
        if (/Network Error|timeout/.test(err)) commit('setNetworkError', true);
      })
      commit('setLoading', false)
      if (!datatmp) return; //萬一datatmp.data沒有值時.
      const  {data} = datatmp;      
      !IsDev && dispatch('cablePublish', {act: 'reload'} )

      // API 報錯時中斷
      if (data.ErrorCode !== '0') return new Promise(resolve => resolve({error: true, msg: data.ErrorMsg}))
      // 更換餐期時中斷
      if (!Number) return new Promise(resolve => resolve({}))
      if (!data.Remark || typeof(data.Remark) !== 'string') return; //萬一Remark沒有值時.
      const waittmp 		= JSON.parse(data.Remark)
      if (!Array.isArray(waittmp) || (Array.isArray(waittmp) && !waittmp.length) ) return new Promise(resolve => resolve({}));
      const wait 		= waittmp[0]
				,timestap 	= moment(wait.CreateDate).unix()
				,hostUrl 		= IsDev ? 'http://localhost:8080/' : `${window.location.origin}/waiting/index.html`
				,infoPage 	= `${hostUrl}#/r/${EnterpriseID}?i=${timestap}&s=${ShopID}`

      // 判斷是否取號時寄簡訊
      const shouldSendSMS = state.waitConfig.RequestWaitSMS === '1'
      const deskID = state.deskTypeList.find(desk => desk.GID === wait.DeskTypeID).DeskTypeCode

			if (shouldSendSMS) {
				const strUrl = infoPage.replace(/&/g, '^');
				deviseFunction('AppSMSMsg', `${wait.Account};${EnterpriseName} ${ShopName} 候位號碼: ${deskID}${Number} ，詳細請前往 ${strUrl}`)
			}

      // 響鈴聲
      deviseFunction('Bell', 'queue_righ')

			const num1 = PadLeft(Number, 3);
			// alert('列印候位單: '+num1);	// @@

      // 列印候位單
      const printParams = {
        EnterpriseName,
        StoreName: ShopName,
        ShopTel: ShopTel,
        MealCount: MealCount.toString(),
        // Number: Number.toString(),
        Number: num1,
        Code: DeskID,
        QRcode: infoPage,
        prinip: state.prinip,
        ticketUrl: `https://web.jh8.tw/waiting/printFiles/${state.prinFolder}/waitticket.prn`,
      }
			IsDev && console.warn('建立候位資料-列印候位單: ', printParams);	// @@
      deviseFunction('prnwait', JSON.stringify(printParams))
      if (/\/dashboard/.test(location.href)) showAlert(`候位資料: ${wait.Account} 建立成功！`)
      return new Promise(resolve => resolve({}))
    },
    // 更新候位資料狀態
    async updateWaitingQueue({dispatch, commit, state, getters}, body) {
      commit('setLoading', true)
      const {reqUrl, reqHead} = state
      const {ShopID, EnterpriseID} = state.baseData
      const baseBody = { ShopID, EnterpriseID, "act": "updwaitingposition" }
      const reqBody = Object.assign({}, baseBody, body)
      const datatmp 	= await axios.post(reqUrl, reqBody, reqHead).catch(err => {
        if (/Network Error|timeout/.test(err)) commit('setNetworkError', true);
      })
      commit('setLoading', false);
      if (!datatmp) return; //萬一datatmp.data沒有值時.
      const  {data} = datatmp;
      //if (!data.Remark || typeof(data.Remark) !== 'string') return; //萬一Remark沒有值時.
      if (data.ErrorCode !== '0') alert(data.ErrorMsg)
     // 沒有用到data取回的值??      
      dispatch('fetchWaitingQueue')
      dispatch('cablePublish', {act: 'reload'} )
    },
    // 取得候位資料
    async fetchWaitingQueue({dispatch, commit, state, getters}, payload) {
      commit('setLoading', true)
      const {reqUrl, reqHead} = state
      let {ShopID, EnterpriseID} = state.baseData
      if (payload) ShopID = payload.ShopID
      if (payload) EnterpriseID = payload.EnterpriseID

      const reqBody = {
        ShopID, EnterpriseID,
        "act": "getwaitingposition",
        // "orderby": "CreateDate desc",
        "orderby": "AlertTime desc",
        "CreateDate": getToday()
		// "CreateDate":"2020-11-08"	// @@
      }

      const datatmp 	= await axios.post(reqUrl, reqBody, reqHead).catch(err => {
				if (/Network Error|timeout/.test(err)) commit('setNetworkError', true);
			})
      commit('setLoading', false);
      if (!datatmp) return; //萬一datatmp.data沒有值時.
      const  {data} = datatmp;
      if (!data.Remark || typeof(data.Remark) !== 'string') return; //萬一Remark沒有值時.      
			const queue 			= JSON.parse(data.Remark)

      // 存入候位資料
      commit('setWaitingQueue', queue)

      // 取餐期資料
      const them = queue.filter(data => data.Name === 'meatime')
				,hasData = !!them.length
      let nCnt	 = 1
      // console.log('取餐期資料-them: ', them.length);	// @@
      if (!hasData) {
				// 如果沒有餐期資料，建立第一個餐期(重設餐期)
				dispatch('createWaitingQueue', {act: "addwaitingposition", Name: "meatime", MealCount: 1});
				showAlert('已切換成餐期1');
      } else {
				// 取得當下的餐期
				nCnt = them.sort((a, b) => b.MealCount - a.MealCount)[0].MealCount
      }

      // 存入當下的餐期
      commit('setMealCount', parseInt(nCnt))
    },
    // todo: 取得看板banner圖片
    async fetchKanbanBanner({dispatch, commit, state, getters}, payload) {
      const {reqUrl, reqHead} = state
      const {ShopID, EnterpriseID} = state.baseData
      const reqBody = { ShopID, EnterpriseID, "act":"getwaitingadpic" }
      const datatmp 	= await axios.post(reqUrl, reqBody, reqHead).catch(err => {
				if (/Network Error|timeout/.test(err)) commit('setNetworkError', true);
			})
      if (!datatmp) return; //萬一datatmp.data沒有值時.
      const  {data} = datatmp;
      if (!data.Remark || typeof(data.Remark) !== 'string') return; //萬一Remark沒有值時.      
			const remarkObj 		= JSON.parse(data.Remark)
		,enterpriseID 	= state.baseData.EnterpriseID
	  //console.log('取得看板banner圖片-enterpriseID: '+enterpriseID, remarkObj);	// @@

      commit('setKanbanBanner', remarkObj)
    },
    // 取得桌號資料
    async fetchWaitingDesk({dispatch, commit, state, getters}, payload) {
      const {reqUrl, reqHead} = state
      const {ShopID, EnterpriseID} = state.baseData
      const reqBody = { ShopID, EnterpriseID, "act":"getwaitingdesk" }
      const datatmp 	= await axios.post(reqUrl, reqBody, reqHead).catch(err => {
				if (/Network Error|timeout/.test(err)) commit('setNetworkError', true);
			})
      if (!datatmp) return; //萬一datatmp.data沒有值時.
      const  {data} = datatmp;
      if (!data.Remark || typeof(data.Remark) !== 'string') return; //萬一Remark沒有值時.
      commit('setWaitingDesk', JSON.parse(data.Remark))
    },
    // 取得候位標籤資料
    async fetchWaitingTag({dispatch, commit, state, getters}, payload) {
      const {reqUrl, reqHead} = state
      const {ShopID, EnterpriseID} = state.baseData
      const reqBody = { ShopID, EnterpriseID, "act":"getwaitingtag" }
      const datatmp 	= await axios.post(reqUrl, reqBody, reqHead).catch(err => {
				if (/Network Error|timeout/.test(err)) commit('setNetworkError', true);
			})
      if (!datatmp) return; //萬一datatmp.data沒有值時.
      const  {data} = datatmp;
      if (!data.Remark || typeof(data.Remark) !== 'string') return; //萬一Remark沒有值時.
      let Remarks = JSON.parse(data.Remark)
      //根據來源,判斷該頁面是否可以顯示該標籤資料

      //if (router.currentRoute.path === '/dashboard') Remarks=Remarks.filter(x=>x.IsDisplayManage); //管理顯示
      if (router.currentRoute.path === '/number_taker') Remarks=Remarks.filter(x=>x.IsDisplayPickup); //取號顯示

      //排序(先依照[優先排序欄位],在按[排序欄位]排列)
      Remarks = Remarks.slice().sort(function(x, y) {
        return ((x.IsPriority === y.IsPriority) ? 0 : x.IsPriority ? -1 : 1) || (x.SortValue-y.SortValue);
      });

      commit('setWaitingTag', Remarks)
    },
    // (顧客用) 取得預約候位資料
    async fetchWaitInfo({dispatch, commit, state}, payload) {
      commit('setLoading', true);
      const {EnterpriseID, ID} = payload
      const {reqUrl, reqHead} = state
      const reqBody = { EnterpriseID, ID, "act":"getwaitingpositionname" }
				// ,curShopID 	= state.baseData.ShopID
				,curShopID 	= GetPara1('s')
      const datatmp 	= await axios.post(reqUrl, reqBody, reqHead).catch(err => {
        if (/Network Error|timeout/.test(err)) commit('setNetworkError', true);
      })
      commit('setLoading', false);
      if (!datatmp) return; //萬一datatmp.data沒有值時.
      const  {data} = datatmp;
      if (!data.Remark || typeof(data.Remark) !== 'string') return; //萬一Remark沒有值時.
      

      let wait;
      try {
				let arrWait = data.Remark ? JSON.parse(data.Remark) : []
				if (curShopID) {
					arrWait = arrWait.filter(one => one.ShopID == curShopID);
				}
      	wait = arrWait.length ? arrWait[0] : null
				IsDev && console.warn('取得預約候位資料-curShopID: '+curShopID, wait);	// @@

      } catch (e) {}

      return new Promise(resolve => {
				const isOK = data.ErrorCode == 0;
				resolve(isOK ? wait : {error: true, msg: data.ErrorMsg});
      })
    },
  },
})
