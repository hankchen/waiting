/** 用Session Storage,暫存本機的東西 */
import store from './store'
// import fetchData from 'src/libs/fetchData.js'

// const EnterpriseID 	= '8493800757'
	// ,baseUrl 		= 'wuhu.jh8.tw'
	// ,publicUrl		= `https://${baseUrl}/public/newsocket.ashx`

/* [SessionStorage] 緩存(HTML5的內建obj) */
let S_Obj = {
	// mac:	 '',
	isWebProd:	 /web/.test(location.host),
	isStaging:	 /staging/.test(location.host),	// !isWebProd
	isDebug:	 /localhost/.test(location.host) || /^192.168/ig.test(location.host),
	isFromTest: function () {
		const v2 = store.state.userData.RtAPIUrl;
		v2 && v2.trim().toLowerCase();
		this.isDebug && console.log('isFromTest-RtAPIUrl: ', v2); // @@
		return /8012test/.test(v2);
	},
    isTestAPI(){
		// return this.isStaging && this.isFromTest();
		return this.isFromTest();
    },
    save(key, v1){
		sessionStorage[key] = (typeof(v1) === 'object') ? JSON.stringify(v1) : v1;
	},
    read(key){
		const value = sessionStorage[key]
		return /{|]/.test(value) ? JSON.parse(value) : value;
		// return (typeof(value) === 'string') ? JSON.parse(value) : value;
	},
    // clear(){
		// sessionStorage.clear(); //刪除所有資料
	// },

};

/* [LocalStorage] 緩存(HTML5的內建obj) */
let L_Obj = {
	use: 'localStorage' in window && window.localStorage !== null,
    save(key, v1){
		localStorage[key] = (typeof(v1) === 'object') ? JSON.stringify(v1) : v1;
	},
    read(key){
		const value = localStorage[key]
		return /{|]/.test(value) ? JSON.parse(value) : value;
		// return (typeof(value) === 'string') ? JSON.parse(value) : value;
	},
    del(key){
		localStorage.removeItem(key);
	},
    clear(){
		localStorage.clear();
	},
};



export { S_Obj, L_Obj }
