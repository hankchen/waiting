// Vue
import Vue from 'vue'
import store from './store'
import router from './router/index.js'
import VueCompositionApi from '@vue/composition-api'
Vue.use(VueCompositionApi)

// WebSocket 套件
import WaitingChannel from './libs/WaitingChannel.js'

// 與殼溝通相關的 function
import { deviseFunction, returnJsInterFace } from './libs/deviseHelper.js'

// components
import App from './App.vue'
import App_tzml from './App_tzml.vue'
import { L_Obj } from './s_obj.js'
import {GetEnterpriseKey, GetEnterpriseKeyFromUrlPath } from './f_waiting.js'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import '@/assets/css/w3.css'

Vue.component('FAI', FontAwesomeIcon)

// css樣式
// import './assets/css/normalize.css'
import '@blueprintjs/core/lib/css/blueprint.css'
import '@blueprintjs/icons/lib/css/blueprint-icons.css'

// 阻止 vue 在啟動時在 console 跳出生成提示
Vue.config.productionTip = false

/*  存入殼相關的設定 */
// 與殼溝通的 callback function 設置
window.returnJsInterFace = (data, cbFn) => returnJsInterFace(data, cbFn)

// 判斷是否在殼裡面(true/false), 並將判斷值存入 store
const isDeviseApp = (typeof(JSInterface) === "object")
var EnterpriseKey;
store.commit('setIsDeviseApp', isDeviseApp)

setTimeout(() => {
  // 設殼的domain,所有跟api操作的統一
  deviseFunction('SetSP', `{"spName":"HoHttp", "spValue":"https://8012.jh8.tw"}`, '')
  // 跟殼取出單機 IP 位置, 並存入 store
  deviseFunction('GetSP', 'prnip', '"cbFbSetPrinip"')
  // 跟殼取出單機格式, 並存入 store
  deviseFunction('GetSP', 'prnfolder', '"cbFbSetPrinFolder"')
  // 跟殼取出企業號資料, 並存入 store
  deviseFunction('GetSP', 'EnterpriseID', '"cbFbSetEnterpriseID"')
  // 如果是看板頁，跟殼取出門店資料，並存入 Store
  if (/\/kanban/.test(location.href)) deviseFunction('GetSP', 'ShopID', '"cbFbSetShopID"')
  if (/\/kanban/.test(location.href)) deviseFunction('GetSP', 'ShopName', '"cbFbSetShopName"')
  // 殼的設定：離開 -> 再進入畫面時，強制重新 loading
  //deviseFunction('setIO', '0', '') //候位app殼沒有支援
}, 200)

setTimeout(() => { 
  EnterpriseKey = GetEnterpriseKeyFromUrlPath(); 
  //alert('mainJsInState='+store.state.baseData.EnterpriseID);
	const L_EID = L_Obj.read('EnterpriseID');
  //alert('mainJs='+L_EID);
  //alert('是否已登錄='+store.getters.isLogin+',EnterpriseID='+store.state.baseData.EnterpriseID+',ShopID='+store.state.baseData.ShopID)

  //如果LocalStorage有值,但store沒值,則將LocalStorage的門店號指給store (解決看版沒自動登錄問題)
  if (/\/kanban/.test(location.href) && L_EID) {
    store.state.baseData.EnterpriseID = L_EID  
  }

  //以下重載是為了要解決把app刷掉後,重新載入時,風格會變成default頁面
  if (L_EID && store.getters.isLogin){
    let themeVar_p = GetEnterpriseKey(L_EID)
    //alert('Router Style='+themeVar_p+',殼的style='+L_Obj.read('themeVar'))

    if (L_Obj.read('themeVar')!=themeVar_p) //如果殼的風格跟router風格不一樣,重新讀取
    {
      L_Obj.save('themeVar',themeVar_p)
      window.location.reload()
    }
  }
  // css樣式(碳佐麻里客製)
  if (EnterpriseKey=='tzml') {
    import('./assets/css/bootstrap5.0.0-beta2.min.css')
    import('./assets/css/tzml.scss')
  } else {
    import('bootstrap-utilities/bootstrap-utilities.css') // ref: https://getbootstrap.com/docs/4.1/utilities/
    import('./assets/css/ProjectStyle.css')
  }  
}, 400);


// vue instance initial
function initVueInstance() {
  
  const vue = new Vue({
    el: '#app',
    store,
    router,
    render: h => { return EnterpriseKey == 'tzml' ? h(App_tzml) : h(App) },
    // Vue 初始化時執行
    created() {
      // 在 Vue instance 裡面可以用 this.$cable 呼叫該 channel
      Object.defineProperty(Vue.prototype, '$cable', { get() { return WaitingChannel } })
    },
  }).$mount('#app')

  // debug 工具: 在 console 上操作 vue instance (只有在測試環境才能開啟)
  if (process.env.NODE_ENV === 'development') window.vue = () => { return vue }
}

// 殼的設定都完成後，再啟動 Vue instance
setTimeout(() => initVueInstance(), 800)