/* eslint-disable */
import { deviseFunction } from '../libs/deviseHelper.js'
import ActionCable from 'actioncable'
import store from '../store'
import router from '../router'

// 訂閱一個 WS 頻道: PosHostChannel
const cable = ActionCable.createConsumer('wss://web-service.jinher.com.tw/cable')
const WaitingChannel = cable.subscriptions.create("PosHostChannel", {
  // 連線成功時觸發
  connected() {
    console.log('WaitingChannel connected')
  },
  // 連線中斷時觸發
  disconnected() {
    console.log('WaitingChannel disconnected')
  },
  // 收到訊息時觸發
  received(data) {
    console.log('WaitingChannel received', data)
    // 資料驗證
    const {EnterpriseID, ShopID, userName} = store.state.baseData
    const {type, act, message, number, EnterpriseID: pEID, ShopID: pSID, userName: pUser} = data.payload

    if (!EnterpriseID || !ShopID || type !== 'waiting' ) return
    if (EnterpriseID.trim().toLowerCase() !== pEID.trim().toLowerCase() || ShopID.trim().toLowerCase() !== pSID.trim().toLowerCase()) return

    // 必須用白名單方式設定觸發的 function
    if (act === 'reload') return store.dispatch('fetchWaitingQueue')
    if (act === 'bell' && router.app.$route.path === '/kanban') {
      // 叫號時，看板的桌號欄要閃爍 (5 秒後關閉)
      store.commit('setDeskNotify', number[0])
      setTimeout(() => store.commit('setDeskNotify', ''), 5000)
      // 響鈴
      deviseFunction('Bell', 'queue_righ')
      // 語音提示
      return deviseFunction('Talk', `在場候位來賓號碼 ${number} 準備入席`)
    }
  },
  // 發送訊息給頻道
  commit(payload) {
    return this.perform('commit', {payload: payload});
  },
});

export default WaitingChannel
