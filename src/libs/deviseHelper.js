/* eslint-disable */
/* 與殼溝通相關的 function */

import store from '../store'
import { L_Obj } from '../fun1.js'	// 共用fun在此!

// 打 logger
const deviseLogger = function(act, params, cbFn) {
  // 時間戳記
  const getTimeStamp = function(t) {
    // 當下時間
    const time = t || new Date()
    // 補齊位數 => e.g. 1 -> 01 ,  51 -> 051
    const pad = function(num, maxLength) {
      // 要補齊的字數
      const diffLength = maxLength - num.toString().length
      return (new Array(diffLength + 1)).join('0') + num
    }
    // => sample: " @ 13:02:02.993"  (時:分:秒.毫秒)
    return  "@ " + (pad(time.getHours(), 2)) + ":" + (pad(time.getMinutes(), 2)) + ":" + (pad(time.getSeconds(), 2)) + "." + (pad(time.getMilliseconds(), 3));
  }

  console.groupCollapsed(`devise: ${act} ${getTimeStamp()}`);
  console.log('%c act',    'color: #afaeae; font-weight: bold', act);
  console.log('%c params', 'color: #9E9E9E; font-weight: bold', params);
  console.log('%c cbFn',   'color: #03A9F4; font-weight: bold', cbFn);
  console.groupEnd();
}

// 執行與殼溝通 function 的統一接口
const deviseFunction = function(act, params='', cbFn=''){
  if (!act) return console.log('deviseFunction: 請輸入 act')
  // 打 logger
  deviseLogger(act, params, cbFn)
  // 必須在殼裡面才能執行
  if (typeof(JSInterface) === 'undefined') return
  // 執行跟殼溝通的 function
  JSInterface.CallJsInterFace(act, params, cbFn)
}

// // (藉由殼 API) 發送手機驗證簡訊
// function sendDeviseSMSreCode(payload) {
  // // payload sample: { phone: '0955111222;;886', SMSSendQuota: '20190514-3' }
  // // 發送手機驗證簡訊
  // deviseFunction('AppSMSReCode', payload.phone, '"cbFnSyncDeviseSMSreCode"')
// }
// // callback function: 儲存(從殼過來的) 簡訊驗證碼
// const cbFnSyncDeviseSMSreCode = function(e) {
  // // 將簡訊驗證碼存入 state.SMS_Config
  // store.commit('setSMSreCode', e)
// }

// 驗證IP Address
function isValidIP(ip) {
    var reg = /^(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d \d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[ 0-4]\d|25[0-5])$/
    return reg.test(ip);
}

// -- 使用 jsInterFace 跟殼取資料的用法(目前有用到的) --


// GetSPAll => 取全部存入殼的資訊
// JSInterface.CallJsInterFace('GetSPAll', '', '')

// // SetSPS            => 設定多值
// data = { "_KEY": "InvType,InvDes,InvVend", "InvType": "InvType", "InvDes": "InvDes", "InvVend": "InvVend" }
// JSON.stringify(data) // '{"_KEY":"InvType,InvDes,InvVend","InvType":"InvType","InvDes":"InvDes","InvVend":"InvVend"}'
// JSInterface.CallJsInterFace('SetSPS', JSON.stringify(data), '');
// // SetSP             => 將資料存入殼裡
// JSInterface.CallJsInterFace('SetSP', '{"spName":"SMSCode", "spValue":"2345222"}', '')
// // GetSP             => 跟殼取資料
// JSInterface.CallJsInterFace('GetSP', 'SMSCode', '')
// // gps               => 取得 GPS 地理資訊
// JSInterface.CallJsInterFace('gps', '', '')
// // GetAppSysConfig   => 取得殼的系統參數
// JSInterface.CallJsInterFace('GetAppSysConfig', 'AppRegNeedSMSCheck,AppSMSIntervalMin,APPSMSDayTimes', '')
// // getAppPublicToken => 取公用接口 token
// JSInterface.CallJsInterFace('getAppPublicToken', '', '')
// // AppSMSForgetPW    => 會改會員的帳號密碼，並將新的密碼簡訊發送給會員
// JSInterface.CallJsInterFace('AppSMSForgetPW', '0956241782', '')
// // AppSMSReCode      => 發送驗證簡訊 => callback 會回傳驗證碼 => 也可以用 '0956241782;hello' 自訂驗證碼
// JSInterface.CallJsInterFace('AppSMSReCode', '0956241782', '' );
// // getBrightness     => 取得目前螢幕亮度
// JSInterface.CallJsInterFace('getBrightness', '', '');
// // setBrightness     => 調整螢幕亮度(最亮值 255)
// JSInterface.CallJsInterFace('setBrightness', '255', '');
// // barcode           => 開啟掃描器
// JSInterface.CallJsInterFace('barcode', '', '');
// // openWeburl        => 開啟外部連結
// JSInterface.CallJsInterFace("openWeburl", link, '')
// // tel               => 啟用撥打電話功能
// JSInterface.CallJsInterFace("tel", '0988123123', '')
// // shareTo           => 啟用分享功能
// JSInterface.CallJsInterFace('shareTo', '{"subject":"給目標的抬頭", "body":"給目標的內容", "chooserTitle":"開啟分享時的抬頭"}', '');
// // setIO             => 離開 -> 再進入畫面時，不用再重新 loading
// JSInterface.CallJsInterFace('setIO', '1', '')
// // onFrontEndInited  => 通知殼：前端已經初始化完畢 (殼才會執行 returnJsInterFace )
// JSInterface.CallJsInterFace('onFrontEndInited', '', '')
// // mailto            => 啟用殼的寄信功能
// JSInterface.CallJsInterFace('mailto', '{"mailsubject":"給目標的抬頭", "mailbody":"給目標的內容", "chooserTitle":"開啟分享時的抬頭", "mailreceiver":"sdlong.jeng@gmail.com"}', '')
// // Do_Register       => 推播設定, 存入 App 推播所需要的參數
// JSInterface.CallJsInterFace('Do_Register', '{"MembersName":"Allen", "Account":"0956241782"}', '')
// // clearchache       => 清除網頁 cache
// JSInterface.CallJsInterFace('clearchache', '', '')

/* callback function 集中於此 */

// 存入出單機 ip
const cbFbSetPrinip = (ip) => { isValidIP(ip)&&store.commit('setPrinip', ip) }
// 存入出單機格式
const cbFbSetPrinFolder = (folder) => { folder.length&&store.commit('setPrinFolder', folder) }
// 存入企業號
const cbFbSetEnterpriseID = (id) => {id.length&&L_Obj.save('EnterpriseID',id)&&store.commit('setEnterpriseID', id) }
// 存入門店號
const cbFbSetShopID = (id) => { id.length&&store.commit('setShopID', id) }
// 存入門店名
const cbFbSetShopName = (id) => { id.length&&store.commit('setShopName', id) }

// -- APP 殼的公用 callback 接口 (所有打向殼的 API，都會由殼裡來 call 此 function，回傳資料) --
// 解說： 跟殼溝通的接口

// sample: JSInterface.CallJsInterFace('GetString', 'Account,mac', console.log('hello'))
// => data: {State: "OK", Type: "GetString", Obj: "177f8e5494d251e62ffd68c9dfe903a2"}
// => 殼會執行 window.returnJsInterFace(data, console.log('hello'))
// 所以此 function 是用來接收殼回傳的資訊，並做對應的邏輯處理

// !!重要!! => 由於 returnJsInterFace 被 public 出去，所以任何人都能在 console 執行 returnJsInterFace 來亂 try
// !!重要!! => 所以在裡面的 callback function 務必設定成白名單形式，只接受合法的，不然會造成資安漏洞
const returnJsInterFace = (data, cbFn) => {
  console.log('returnJsInterFace receive:', data, ", cbFn:", cbFn)

  try {
    // data sample:
    // {State: "OK", Type: "gps", Obj: "{Lat: 37.785835, Lng: -122.406418}"}
    // {State: "OK", Type: "GetAppSysConfig", Obj: {AppRegNeedSMSCheck: "false", AppSMSIntervalMin: "10", APPSMSDayTimes: "3", AppSMSReCode: "4"}}

    // 依據 Type 不同執行各自的邏輯，這樣設計的用意是做白名單，防止被亂 try
    // Policy:
    //   1. !!重要!! 取得資料後要執行的邏輯一律做在 callback function (e.g. 拿取得的資料存入 store, 打 api , 做 xxx 事情)
    //   2. !!重要!! 這裏只做從殼回傳資料的『驗證與轉換』
    //   3. 在 iOS 的 cbFn 是 return fuction, 在 Android 是 return string
    switch(data.Type) {
      // 跟殼取設定值
      case 'GetSP':        
        // (有 cbFn) 必須符合特定參數，才執行該 callback function (防止被亂 try )
        if (cbFn === 'cbFbSetPrinip') cbFbSetPrinip(data.Obj)
        if (cbFn === 'cbFbSetPrinFolder') cbFbSetPrinFolder(data.Obj)
        if (cbFn === 'cbFbSetEnterpriseID') cbFbSetEnterpriseID(data.Obj) 
        if (cbFn === 'cbFbSetShopID') cbFbSetShopID(data.Obj)
        if (cbFn === 'cbFbSetShopName') cbFbSetShopName(data.Obj)
      break;
      // // 發送簡訊驗證碼
      // case 'AppSMSReCode':
        // // (有 cbFn) 必須符合特定參數，才執行該 callback function (防止被亂 try )
        // if (cbFn === 'cbFnSyncDeviseSMSreCode') cbFnSyncDeviseSMSreCode(data.Obj)
      // break;

      // 不做任何 callback 的 act
      case 'SetSP': case 'GetSPAll': case 'AppSMSMsg':
      break;

      // 其他未設定到的，打 console.warn 來做提醒
      default:
        console.warn('type not defined :', data.Type)

    }
  } catch(error) {
    // 若有任何例外錯誤，要跳 console.error 以利查 bug
    console.error('returnJsInterFace Exception Error: ', error);
  }
}

export { deviseFunction, returnJsInterFace }
