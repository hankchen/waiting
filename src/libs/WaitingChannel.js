/* eslint-disable */
import { deviseFunction } from '../libs/deviseHelper.js'
import store from '../store'
import router from '../router'
import Paho from '../libs/mqttws31.min.js'
//import '../libs/paho-mqtt.js'

import { currentTime2 } from '../libs/timeHelper.js'

// 訂閱一個 WS 頻道: PosHostChannel

var  timeout = 5,
keepAlive = 100,
cleanSession = false,
ssl = true;
const clientId = makeid();
  var userName="5a2j5rKz";
  var password="ajcwNTk1MTQ1";
  var cable = new Paho.MQTT.Client("wss://mqtt.jh8.tw:11883/",clientId);

// 指定收到訊息時的處理動作
cable.onMessageArrived = onMessageArrived;
cable.onConnectionLost = onConnectionLost;


var options = {
    invocationContext: {
        //     host: hostname,
        //     port: port,
        path: '/', //cable.path,
        clientId: clientId
    },
    timeout: timeout,
    keepAliveInterval: keepAlive,
    cleanSession: cleanSession,
    useSSL: ssl,
    userName: userName,
    password: password,
    onSuccess: onConnect,
    onFailure: function (e) {
      console.log('outside onFailure ===> start')
      console.log(e);
      // alert('outside onFailure ===>' + JSON.stringify(e) + '<===')
      var  s = "{time:" + currentTime2() + ", onFailure()}";
      console.log(s);
      doRetry() // 沒有測試過此行,因實際上ipad網路斷線不會跑到這裡,但還是先加上去
    }
};
//timeout userName password willMessage keepAliveInterval cleanSession useSSL invocationContext onSuccess onFailure hosts ports mqttVersion
// 連接 MQTT broker
// cable.connect({onSuccess:onConnect});
console.log('======> wss start to connect (first)')
cable.connect(options);
const WaitingChannel = {
    destinationName:"PosHostChannel",
    commit: function commit(payload) {
        console.log('WaitingChannel commit: ', payload);
        console.log('WaitingChannel destinationName: ',  this.destinationName);
        var message = new Paho.MQTT.Message(JSON.stringify(payload));
        //   message.destinationName = TOPIC + "text";
        message.destinationName = this.destinationName;
       // cable.wireMessage = message;
        // client.send(message);
        return cable.send(message);
    },
    commit_subscribe: function commit(payload,subscribe) {
        console.log('WaitingChannel commit: ', payload);
        var message = new Paho.MQTT.Message(JSON.stringify(payload));
        //   message.destinationName = TOPIC + "text";

        message.destinationName = subscribe;
        // client.send(message);
       // cable.wireMessage = message;
        return cable.send(message);
    },
    subscribe: function subscribe(Channel)
    {
        console.log('WaitingChannel subscribe: ', Channel);
        this.destinationName = Channel;
        cable.subscribe(Channel);
    },
    unsubscribe: function subscribe(Channel)
    {
        console.log('WaitingChannel subscribe: ', Channel);
 
        cable.unsubscribe(Channel);
    }
    ,
    isConnected: function isConnected()
    {
        return  cable.isConnected();
    }
    ,
    ReConnect: function ReConnect(subscribe)
    {
        this.destinationName = subscribe;
        options = {
            invocationContext: {
                //     host: hostname,
                //     port: port,
                path: '/', //cable.path,
                clientId: clientId
            },
            timeout: timeout,
            keepAliveInterval: keepAlive,
            cleanSession: cleanSession,
            useSSL: ssl,
            userName: userName,
            password: password,
            onSuccess: onConnect,
            onFailure: function (e) {
              console.log('ReConnect onFailure ===> start')
              console.log(e);
              // alert('ReConnect onFailure ===>' + JSON.stringify(e) + '<===')
              console.log(e);
              var  s = "{time:" + currentTime2() + ", onFailure()}";
              console.log(s);
              doRetry() // 沒有測試過此行,因實際上ipad網路斷線不會跑到這裡,但還是先加上去
            }
        };
             console.log('======> wss start to connect (ReConnect)')
             cable.connect(options);
    }
}

function onConnect() {
    console.log('onConnect ===> start')
    // 確認連接後，才能訂閱主題
    console.log("onConnect then subscribe topic");
    cable.subscribe("PosHostChannel");
    // 如果已登入=>重連私人頻道
    if (store.getters.isLogin) {
      console.log('onConnect ===> isLogin => cable.subscribe('
                     +"Waiting"+store.state.baseData.EnterpriseID+""+store.state.baseData.ShopID
                     +')')
      cable.subscribe("Waiting"+store.state.baseData.EnterpriseID+""+store.state.baseData.ShopID)  
    }
}
function doRetry() {
  console.log('doRetry ===> start')
  // alert('doRetry ===> start')
  setTimeout(()=>{
    
    cable = new Paho.MQTT.Client("wss://mqtt.jh8.tw:11883/",clientId);
    options = {
      invocationContext: {
          //     host: hostname,
          //     port: port,
          path: '/',
          clientId: clientId
      },
      timeout: timeout,
      keepAliveInterval: keepAlive,
      cleanSession: cleanSession,
      useSSL: ssl,
      userName: userName,
      password: password,
      onSuccess: onConnect,
      onFailure: function (e) {
        console.log('doRetry ===> onFailure')
        console.log(e);
        // alert('onConnectionLost onFailure ===>' + JSON.stringify(e) + '<===')
        var  s = "{time:" + currentTime2() + ", onFailure()}";
        console.log(s);
        doRetry(cable)
      }
    };
    
    // 指定收到訊息時的處理動作
    cable.onMessageArrived = onMessageArrived;
    cable.onConnectionLost = onConnectionLost;
    console.log('======> wss start to connect (doRetry)')
    cable.connect(options);
    
  }, 2000)
}
function onConnectionLost(responseObject) {
    console.log(responseObject);
    var s = "{time:" + currentTime2() + ", onConnectionLost()}";
    console.log(s);
    if (responseObject.errorCode !== 0) {
        console.log("onConnectionLost:" + responseObject.errorMessage);
        console.log("連接已斷開");

        // alert('onConnectionLost ===>' + JSON.stringify(responseObject) + '<===')

        // if (responseObject.errorCode == 5 || responseObject.errorCode == 7
        //   || responseObject.errorCode == 8) 
        // {
       // cable = new Paho.MQTT.Client("wss://mqtt.jh8.tw:11883/",clientId);
       // cable.onMessageArrived = onMessageArrived;
       // cable.onConnectionLost = onConnectionLost;
       cable = new Paho.MQTT.Client("wss://mqtt.jh8.tw:11883/",clientId);
       options = {
        invocationContext: {
            //     host: hostname,
            //     port: port,
            path: '/', //cable.path,
            clientId: clientId
        },
        timeout: timeout,
        keepAliveInterval: keepAlive,
        cleanSession: cleanSession,
        useSSL: ssl,
        userName: userName,
        password: password,
        onSuccess: onConnect,
        onFailure: function (e) {
          console.log(e);
          // alert('onConnectionLost onFailure ===>' + JSON.stringify(e) + '<===')
          var  s = "{time:" + currentTime2() + ", onFailure()}";
          console.log(s);
          doRetry() // 必要!ipad實際網路斷線會跑到這裡
        }
    };
// 指定收到訊息時的處理動作
cable.onMessageArrived = onMessageArrived;
cable.onConnectionLost = onConnectionLost;
         console.log('======> wss start to connect (lost)')
         cable.connect(options);
        // cable.connect();
        // }
    }
}
// 收到訊息時...
function onMessageArrived(messages) {
    console.log("onMessageArrived:"+messages.destinationName);
    console.log("onMessageArrived:"+messages.payloadString);
 
    var data = JSON.parse(messages.payloadString);
    console.log('WaitingChannel received', data);
    // 資料驗證
   // 資料驗證
   const {EnterpriseID, ShopID, userName} = store.state.baseData
   //const {type, act, message, number, EnterpriseID: pEID, ShopID: pSID, userName: pUser} = data.payload
   const {type, act, message, number, EnterpriseID: pEID, ShopID: pSID, userName: pUser} = data

   if (!EnterpriseID || !ShopID || type !== 'waiting' ) return
   if (EnterpriseID.trim().toLowerCase() !== pEID.trim().toLowerCase() || ShopID.trim().toLowerCase() !== pSID.trim().toLowerCase()) return

   // 必須用白名單方式設定觸發的 function
   if (act === 'reload') return store.dispatch('fetchWaitingQueue')
   if (act === 'bell' && router.app.$route.path === '/kanban') {
     // 叫號時，看板的桌號欄要閃爍 (5 秒後關閉)
     store.commit('setDeskNotify', number[0])
     setTimeout(() => store.commit('setDeskNotify', ''), 5000)
     // 響鈴
     deviseFunction('Bell', 'queue_righ')
     // 語音提示
     return deviseFunction('Talk', `在場候位來賓號碼。${number}。準備入席`)
   }

}
function makeid() {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    
    for (var i = 0; i < 15; i++)
    text += possible.charAt(Math.floor(Math.random() * possible.length));
    
    return text;
}
export default WaitingChannel
