import moment from 'moment'

// 取得當下時間
const currentTime = () => {
  return moment(new Date()).format('YYYY-MM-DD HH:mm:ss')
}
const currentTime2 = () => {
  return moment(new Date()).format('yyyy-MM-dd hh:mm:ss')
}

/** @Important: ※有陷阱：當心跨日問題,醬一來日期永遠定住,改成每次取 */
// 預設日期: 當天 (格式： '2019-09-19')
// const today = moment(new Date()).format('YYYY-MM-DD')
const getToday = () => {
  return moment(new Date()).format('YYYY-MM-DD')
}

// export { moment, currentTime, currentTime2, today, getToday }
export { moment, currentTime, currentTime2, getToday }
