/* app 通用 helper  */

// Vuex Store
import store from '../store'

// toast
import { Toaster } from "@blueprintjs/core"
const AppToaster = Toaster.create({ position: 'top' })

// 觸發 Toast (訊息通知元件)
function showToast(message, config = {}) {
  const baseConfig = {intent: 'primary', icon: 'notifications' }
  const data = Object.assign({}, baseConfig, config, {message: message})
  return AppToaster.show(data)
  /**
    # 用法：

    * 一般
    => app.showToast('hello world')

    * 加入客制色系 & icon 的用法 (預設: {intent: 'primary', icon: 'notifications' } )
    => app.showToast('hello world', {intent: 'success', icon: 'issue' })

    * 可用參數：
    intent : primary, success, danger, warning
    icon   : 參考 https://blueprintjs.com/docs/#icons
  **/
}

function setLoading(status) { store.commit('setLoading', status) }

// 顯示自訂的 alert 畫面
function showAlert(text) {
  store.commit('setCustomModal', { type: 'alert', text })
  setTimeout(() => store.commit('setCustomModal', { type: '', text: '' }), 3000); 
}

// 顯示自訂的 confirm 畫面
function showConfirm(text, confirmText, cancelText) {
  return new Promise((resolve, reject) => {
    store.commit("setCustomModal", {
      type: 'confirm',
      text, confirmText, cancelText, resolve, reject
    });
  });
}

// 顯示自訂的 prompt 畫面
function showPrompt(text) {
  return new Promise((resolve, reject) => {
    store.commit("setCustomModal", {
      type: 'prompt', text, resolve, reject
    })
  })
}

export { showToast, showAlert, showConfirm, setLoading, showPrompt }
