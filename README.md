# waiting

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn run serve
```

### Compiles and minifies for production
```
yarn run build
```

### Run your tests
```
yarn run test
```

### Lints and fixes files
```
yarn run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).


--

## 使用解說:
1. 系統必須安裝python,編碼Sass會需要
2. 如果遇到Yarn編譯Sass錯誤請下指令yarn add sass -D

用 source tree 打開此專案, 點擊右上角的『Terminal』，打開終端機視窗

輸入 `yarn; yarn serve` 安裝 & 執行開發環境

## 打包並更新至正式站/測試站

複製以下的網址並執行即可

正式站：
https://jenkins.jinher.com.tw/generic-webhook-trigger/invoke?token=waiting

測式站：
https://jenkins.jinher.com.tw/generic-webhook-trigger/invoke?token=waiting-s
